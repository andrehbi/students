package restaurant;

/**
 * A group (of people) dining together, and should be seated at the same table.
 */
public class Group {

	/**
	 * Initializes this Group with the provided guest count
	 * @param guestCount
	 */
	public Group(int guestCount) {
		this.guestCount = guestCount;
	}

	// TODO Add necessary code
}
