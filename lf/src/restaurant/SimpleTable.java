package restaurant;

public class SimpleTable extends Table {
	
	private int capacity;
	private final int ID;
	private static int nextID = 1;

	public SimpleTable(int capacity) {
		this.ID = SimpleTable.nextID++; // Setter id = den statiske variabelen nextID og inkrementerer samtidig sistnevnte
		this.capacity = capacity;
	}

	@Override
	public int getCapacity() {
		return capacity;
	}

	public int getID() {
		return ID;
	}

}
