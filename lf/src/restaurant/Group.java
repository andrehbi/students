package restaurant;

/**
 * A group (of people) dining together, and should be seated at the same table.
 */
public class Group {
	
	private Table table;
	private int guestCount;

	/**
	 * Initializes this Group with the provided guest count
	 * @param guestCount
	 */
	public Group(int guestCount) {
		this.guestCount = guestCount;
	}

	public Group(int guestCount, Table table) {
		this.guestCount = guestCount;
		this.table = table;
	}

	public Table getTable() {
		return table;
	}
	
	public void setTable(Table table) {
		Table oldtable = this.table;
		if (table != null) {
			if (this.getGuestCount() > table.getCapacity()) {
				throw new IllegalArgumentException("Table does not have enough capacity for this group");
			}
			this.table = table;
			if (table.getGroup() != this) {
				table.setGroup(this);
			}
		}
		else {
			this.table = table;
		}
		if (oldtable != null && oldtable.getGroup() == this) {
			oldtable.setGroup(null);
		}
	}

	public int getGuestCount() {
		return guestCount;
	}

}
