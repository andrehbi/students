package restaurant;

public abstract class Table implements Comparable<Table>{
	
	private Group group;
	
	public abstract int getCapacity();

	public Group getGroup() {
		return group;
	}
	
	public void setGroup(Group group) {
		Group oldgroup = this.group;
		if (group != null) {
			if (group.getGuestCount() > this.getCapacity()) {
				throw new IllegalArgumentException("Table does not have enough capacity for this group");
			}
			this.group = group;
			if (group.getTable() != this) group.setTable(this);
		}
		else {
			this.group = group;
		}
		if (oldgroup != null && oldgroup.getTable()==this) {
			oldgroup.setTable(null);
		}
	}

	public int compareTo(Table table) {
		return this.getCapacity()-table.getCapacity();
	}

}
