package eksamensforelesning.lf;

public interface ListListener {

	public void listChanged(Patient patient);
	
}
