package oppsummeringsforelesning.lf;

import java.util.ArrayList;
import java.util.List;

public class MultipleChoiceQuestion implements Question {

	private String questionText;
	private List<String> choices;
	private int correctAnswer;
	private Exam exam;
	private int taskNumber;
	
	private static int counter = 10000;
	
	public MultipleChoiceQuestion(String questionText, List<String> choices, int correctAnswer) {
		if (choices.size() < 2) {
			throw new IllegalArgumentException("The number of choices must be greater than 1");
		}
		
		if (correctAnswer < 0 || correctAnswer >= choices.size()) {
			throw new IllegalArgumentException("There is no correct answer");
		}
		
		this.questionText = questionText;
		this.choices = new ArrayList<>(choices);
		this.correctAnswer = correctAnswer;
		
		taskNumber = counter++;
	}
	
	
	@Override
	public void setExam(Exam exam) {
		if (this.exam == exam) {
			return;
		}
		
		if (this.exam != null) {
			this.exam.removeQuestion(this);
		}
		
		this.exam = exam;
		
		if (exam != null) {
			exam.addQuestion(this);
		}
	}

	@Override
	public int getTaskNumber() {
		return taskNumber;
	}
	
	public String getQuestion() {
		return questionText;
	}
	
	public List<String> getAnswers() {
		return new ArrayList<>(choices);
	}
	
	public int getCorrectAnswer() {
		return correctAnswer;
	}

}
