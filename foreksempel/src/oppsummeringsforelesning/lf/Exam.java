package oppsummeringsforelesning.lf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Exam implements Iterable<Question> {

	private Collection<Question> questions = new ArrayList<>();
	
	public Exam() {
		
	}
	
	public Exam(String fileName) throws FileNotFoundException {
		loadQuestionFromFile(fileName);
	}
	
	public Collection<Question> getQuestions() {
		return new ArrayList<>(questions);
	}
	
	public void addQuestion(Question question) {
		if (!questions.contains(question)) {
			questions.add(question);
			
			question.setExam(this);
		}
	}
	
	public void removeQuestion(Question question) {
		if (questions.contains(question)) {
			questions.remove(question);
			
			question.setExam(null);
		}
	}
	
	public void saveToFile(String fileName, boolean withAnswers) throws FileNotFoundException {
		try (PrintWriter writer = new PrintWriter("src/oppsummeringsforelesning/" + fileName + ".txt")) {
			
			if (withAnswers) {
				writer.println("LF");
			} else {
				writer.println("Exam");
			}
			
			for (Question question : this) {
				if (question instanceof TextAnswerQuestion) {
					TextAnswerQuestion textAnswerQuestion = (TextAnswerQuestion) question;
					writer.println("Text answer question");
					
					writer.println(textAnswerQuestion.getQuestion());
					
					if (withAnswers) {
						writer.println(textAnswerQuestion.getAnswer());
					}
				} else if (question instanceof MultipleChoiceQuestion) {
					MultipleChoiceQuestion multipleChoiceQuestion = (MultipleChoiceQuestion) question;
					writer.println("Multiple choice question");
					
					writer.println(multipleChoiceQuestion.getQuestion());
					
					if (withAnswers) {
						writer.println(multipleChoiceQuestion.getAnswers().size());
					}
					multipleChoiceQuestion.getAnswers().forEach(answer -> writer.println(answer));
					
					if (withAnswers) {
						writer.println(multipleChoiceQuestion.getCorrectAnswer());
					}
				}
			}
			
		}
	}
	
	private void loadQuestionFromFile(String fileName) throws FileNotFoundException {
		try (Scanner scanner = new Scanner(new File("src/oppsummeringsforelesning/" + fileName + ".txt"))) {
			if (!scanner.nextLine().equals("LF")) {
				throw new IllegalArgumentException("File is not in the right format");
			}
			
			while (scanner.hasNextLine()) {
				switch (scanner.nextLine()) {
					case "Text answer question":
						TextAnswerQuestion textAnswerQuestion = new TextAnswerQuestion(scanner.nextLine(), scanner.nextLine());
						addQuestion(textAnswerQuestion);
						break;
					case "Multiple choice question":
						String questionText = scanner.nextLine();
						int numberOfChoices = Integer.valueOf(scanner.nextLine());
						
						List<String> choices = IntStream.range(0, numberOfChoices)
								 						.mapToObj(i -> scanner.nextLine())
								 						.collect(Collectors.toList());
						
						int correctAnswer = Integer.valueOf(scanner.nextLine());
						
						MultipleChoiceQuestion multipleChoiceQuestion = new MultipleChoiceQuestion(questionText, choices, correctAnswer);
						addQuestion(multipleChoiceQuestion);
						
						break;
					default:
						throw new IllegalStateException("File format is not supported");
				}
			}
		}
	}

	@Override
	public Iterator<Question> iterator() {
		return new ExamIterator(this);
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		Exam exam = new Exam();
		TextAnswerQuestion question1 = new TextAnswerQuestion("Spørsmål A", "Svar A");
		TextAnswerQuestion question2 = new TextAnswerQuestion("Spørsmål B", "Svar B");
		exam.addQuestion(question1);
		exam.addQuestion(question2);
		
		MultipleChoiceQuestion question3 = new MultipleChoiceQuestion("Spørsmål C", Arrays.asList("Alternativ 1", "Alternativ 2", "Alternativ 3"), 1);
		exam.addQuestion(question3);
		
		exam.saveToFile("test", true);
		
		//Exam exam2 = new Exam("test");
		//TextAnswerQuestion question3 = new TextAnswerQuestion("Spørsmål C", "Svar C");
		//exam2.addQuestion(question3);
		//exam2.saveToFile("test2", true);
	}
	
}
