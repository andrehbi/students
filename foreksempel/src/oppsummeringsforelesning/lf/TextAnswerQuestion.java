package oppsummeringsforelesning.lf;

public class TextAnswerQuestion implements Question {

	private String questionText;
	private String correctAnswer;
	private Exam exam;
	private int questionNumber;
	
	private static int counter = 0;
	
	public TextAnswerQuestion(String questionText, String correctAnswer) {
		this.questionText = questionText;
		this.correctAnswer = correctAnswer;
		
		questionNumber = counter++;
		
		//questionNumber = counter;
		//counter++;
	}
	
	public void setExam(Exam exam) {
		if (this.exam == exam) {
			return;
		}
		
		if (this.exam != null) {
			this.exam.removeQuestion(this);
		}
		
		this.exam = exam;
		
		if (exam != null) {
			exam.addQuestion(this);
		}
	}
	
	public String getQuestion() {
		return questionText;
	}
	
	public String getAnswer() {
		return correctAnswer;
	}

	@Override
	public int getTaskNumber() {
		return questionNumber;
	}
	
}
