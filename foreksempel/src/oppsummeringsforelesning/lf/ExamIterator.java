package oppsummeringsforelesning.lf;

import java.util.Iterator;

public class ExamIterator implements Iterator<Question> {

	private Iterator<Question> questions;
	
	public ExamIterator(Exam exam) {
		questions = exam.getQuestions().stream()
						   			   .sorted((o1, o2) -> {
			if (o1.getClass().equals(o2.getClass())) {
				return o1.getTaskNumber() - o2.getTaskNumber();
			}
			
			if (o1 instanceof MultipleChoiceQuestion) {
				return -1;
			}
			
			return 1;
		}).iterator();
	}
	
	@Override
	public boolean hasNext() {
		return questions.hasNext();
	}

	@Override
	public Question next() {
		return questions.next();
	}

}
