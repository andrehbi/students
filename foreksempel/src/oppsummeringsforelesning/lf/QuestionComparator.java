package oppsummeringsforelesning.lf;

import java.util.Comparator;

public class QuestionComparator implements Comparator<Question> {

	@Override
	public int compare(Question o1, Question o2) {
		if (o1.getClass().equals(o2.getClass())) {
			return o1.getTaskNumber() - o2.getTaskNumber();
		}
		
		if (o1 instanceof MultipleChoiceQuestion) {
			return -1;
		}
		
		return 1;
	}

}
