package oppsummeringsforelesning.lf;

public interface Question {

	void setExam(Exam exam);
	int getTaskNumber();
	
}
